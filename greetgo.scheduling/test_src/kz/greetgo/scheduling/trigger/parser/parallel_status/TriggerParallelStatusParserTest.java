package kz.greetgo.scheduling.trigger.parser.parallel_status;

import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.TriggerParseError;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TriggerParallelStatusParserTest {

  @DataProvider
  public Object[][] maxParallelSize() {
    return new Object[][]{
      {"параллельно повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", null},
      {"параллельно [5] повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", 5},
    };
  }

  @Test(dataProvider = "maxParallelSize")
  public void getMaxParallelSize(String triggerString, Integer expectedSize) {
    List<TriggerParseError> errors = new ArrayList<>();

    //
    //
    Integer actualSize = TriggerParallelStatusParser.getMaxParallelSize(triggerString, 11, errors);
    //
    //

    assertThat(errors).isEmpty();
    assertThat(actualSize).isEqualTo(expectedSize);
  }

  @DataProvider
  public Object[][] maxParallelSize__errors() {
    return new Object[][]{
      {"параллельно [] повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", "Пустое значение в квадратных скобках не допустимо."},
      {"параллельно [5 повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", "Квадратные скобки не закрыты: [~"},
    };
  }

  @Test(dataProvider = "maxParallelSize__errors")
  public void getMaxParallelSize__errors(String triggerString, String expectedErrorMessage) {
    List<TriggerParseError> errors = new ArrayList<>();

    //
    //
    Integer actualSize = TriggerParallelStatusParser.getMaxParallelSize(triggerString, 11, errors);
    //
    //

    assertThat(actualSize).isNull();
    assertThat(errors).hasSize(1);

    TriggerParseError triggerParseError = errors.get(0);
    assertThat(triggerParseError.errorMessage()).isEqualTo(expectedErrorMessage);
    assertThat(triggerParseError.triggerString()).isEqualTo(triggerString);
  }

  @DataProvider
  public Object[][] parallelRange() {
    return new Object[][]{
      {"параллельно повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", Range.of(11, 81)},
      {"параллельно [5] повторять каждые 30 минут * (понедельник + среда) * с 10:00 до 18:00 ", Range.of(15, 85)},
    };
  }

  @Test(dataProvider = "parallelRange")
  public void getParallelRange(String triggerString, Range expectedRange) {

    //
    //
    Range actualRange = TriggerParallelStatusParser.getParallelRange(triggerString, 11);
    //
    //

    assertThat(actualRange).isEqualTo(expectedRange);
  }

}