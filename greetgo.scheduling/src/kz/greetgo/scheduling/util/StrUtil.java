package kz.greetgo.scheduling.util;

import lombok.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class StrUtil {

  public static @NonNull String toLenZero(long number, int len) {
    StringBuilder sb = new StringBuilder();
    sb.append(number);
    while (sb.length() < len) {
      sb.insert(0, '0');
    }
    return sb.toString();
  }

  public static @NonNull String toLenSpace(Object object, int len) {
    StringBuilder sb = new StringBuilder();
    sb.append(object);
    while (sb.length() < len) {
      sb.insert(0, ' ');
    }
    return sb.toString();
  }

  public static @NonNull String mul(String s, int times) {
    return String.valueOf(s).repeat(Math.max(0, times));
  }

  public static String streamToStr(@NonNull InputStream inputStream) {
    try {
      ByteArrayOutputStream out    = new ByteArrayOutputStream();
      byte[]                buffer = new byte[1024 * 4];
      while (true) {
        int count = inputStream.read(buffer);
        if (count < 0) {
          return out.toString(StandardCharsets.UTF_8);
        }
        out.write(buffer, 0, count);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
