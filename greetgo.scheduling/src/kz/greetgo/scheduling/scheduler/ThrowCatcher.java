package kz.greetgo.scheduling.scheduler;

@FunctionalInterface
public interface ThrowCatcher {

  void catchThrowable(Throwable throwable);

}
