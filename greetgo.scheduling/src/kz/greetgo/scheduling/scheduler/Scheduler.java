package kz.greetgo.scheduling.scheduler;

import kz.greetgo.scheduling.trigger.Trigger;
import lombok.NonNull;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class Scheduler {
  private final List<TaskHolder> taskHolderList;

  private final long pingDelayMillis;

  Scheduler(List<TaskHolder> taskHolderList, long pingDelayMillis) {
    this.taskHolderList  = taskHolderList;
    this.pingDelayMillis = pingDelayMillis;
  }

  // TODO это поле показывать в SchedulerStateInfo
  private final AtomicBoolean working = new AtomicBoolean(true);

  // TODO это поле показывать в SchedulerStateInfo
  private long lastMillis;
  // TODO это поле показывать в SchedulerStateInfo
  private long schedulerStartedAtMillis;

  // TODO это поле показывать в SchedulerStateInfo
  private final AtomicLong workingCounter = new AtomicLong(0);
  // TODO это поле показывать в SchedulerStateInfo
  private final AtomicLong runTaskCounter = new AtomicLong(0);

  // TODO это поле показывать в SchedulerStateInfo
  private final AtomicBoolean wasInterrupted = new AtomicBoolean(false);

  private final AtomicReference<Thread> mainSchedulerThread = new AtomicReference<>(null);

  public void shutdown() {
    working.set(false);
  }

  public void startup() {

    schedulerStartedAtMillis = System.currentTimeMillis();
    lastMillis               = schedulerStartedAtMillis - 1000L;

    mainSchedulerThread.set(new Thread(() -> {

      while (working.get()) {

        tryRunTasks();

        if (pingDelayMillis > 0) {
          try {
            Thread.sleep(pingDelayMillis);
          } catch (InterruptedException e) {
            working.set(false);
            wasInterrupted.set(true);
          }
        }

        workingCounter.incrementAndGet();

      }

    }));

    mainSchedulerThread.get().setName("greetgo! Scheduler Thread");
    mainSchedulerThread.get().start();

  }

  private void tryRunTasks() {

    long current = System.currentTimeMillis();

    //TODO показать также все запущенные задачи и сколько раз они запущены и в каком пуле
    for (TaskHolder taskHolder : taskHolderList) {

      Trigger trigger = taskHolder.task.trigger();

      // задача уже выполняется и не проставлен параллельность - выходим
      if (taskHolder.runCount.get() > 0 && !trigger.isParallel()) {
        continue;
      }

      // ограничение проставлен и параллельных задач больше - выходим
      if (trigger.maxParallelSize() != null && taskHolder.runCount.get() >= trigger.maxParallelSize()) {
        continue;
      }

      if (!trigger.isHit(schedulerStartedAtMillis, lastMillis, current)) {
        continue;
      }

      taskHolder.runCount.incrementAndGet();

      execute(taskHolder);

      runTaskCounter.incrementAndGet();

    }

    lastMillis = current + 1;

  }


  public void execute(TaskHolder taskHolder) {
    new Thread(() -> executeTaskNowAndHere(taskHolder), newThreadName(taskHolder)).start();
  }


  private final AtomicInteger nextThreadIndex = new AtomicInteger(1);

  private @NonNull String newThreadName(@NonNull TaskHolder taskHolder) {
    return "greetgo-scheduler-" + taskHolder.task.id() + "-run-" + nextThreadIndex.getAndIncrement();
  }

  private void executeTaskNowAndHere(TaskHolder taskHolder) {
    try {
      try {
        taskHolder.task.job().execute();
      } catch (Throwable throwable) {
        taskHolder.throwCatcher.catchThrowable(throwable);
      }
    } finally {
      taskHolder.runCount.decrementAndGet();
    }
  }
}
