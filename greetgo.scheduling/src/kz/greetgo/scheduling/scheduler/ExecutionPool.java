package kz.greetgo.scheduling.scheduler;

import lombok.Getter;
import lombok.NonNull;

import java.util.concurrent.atomic.AtomicInteger;

public class ExecutionPool {
  public static final String DEF_POOL_NAME = "default";

  // TODO это поле показывать в SchedulerStateInfo
  @Getter
  private final String poolName;

  public ExecutionPool(String poolName) {
    this.poolName = poolName;
  }

  public void execute(TaskHolder taskHolder) {
    new Thread(() -> executeTaskNowAndHere(taskHolder), newThreadName()).start();
  }

  private final AtomicInteger nextThreadIndex = new AtomicInteger(1);

  private @NonNull String newThreadName() {
    return "pool-" + poolName + "-" + nextThreadIndex.getAndIncrement();
  }

  private void executeTaskNowAndHere(TaskHolder taskHolder) {
    try {
      try {
        taskHolder.task.job().execute();
      } catch (Throwable throwable) {
        taskHolder.throwCatcher.catchThrowable(throwable);
      }
    } finally {
      taskHolder.runCount.decrementAndGet();
    }
  }

}
