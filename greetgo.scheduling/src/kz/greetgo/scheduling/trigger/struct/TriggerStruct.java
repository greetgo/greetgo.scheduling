package kz.greetgo.scheduling.trigger.struct;

import kz.greetgo.scheduling.trigger.Trigger;
import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.TriggerParseError;

import java.util.List;

public interface TriggerStruct extends ExpressionElement {

  Trigger trigger();

  List<TriggerParseError> errors(Range top, String triggerString);

}
