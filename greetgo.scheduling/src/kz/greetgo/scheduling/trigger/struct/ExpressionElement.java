package kz.greetgo.scheduling.trigger.struct;

import kz.greetgo.scheduling.trigger.model.Range;

public interface ExpressionElement {

  Range range();

}
