package kz.greetgo.scheduling.trigger.struct.str_lexer;

public enum LexType {
  TIME_VALUE, AFTER_PAUSE, FROM, TO, EVERY, TIME_OF_DAY, WEEK_DAY, AT, REPEAT, MONTH, DAY, DIGIT, RANGE_DELIMITER, YEAR,
}
