package kz.greetgo.scheduling.trigger.struct;

import kz.greetgo.scheduling.trigger.SilentTrigger;
import kz.greetgo.scheduling.trigger.Trigger;
import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.TriggerParseError;

import java.util.Collections;
import java.util.List;

public class TriggerStructEmpty implements TriggerStruct {

  @Override
  public Range range() {
    return new Range(0, 0);
  }

  private final Trigger trigger = new SilentTrigger();

  @Override
  public Trigger trigger() {
    return trigger;
  }

  @Override
  public String toString() {
    return "EMPTY";
  }

  @Override
  public List<TriggerParseError> errors(Range top, String triggerString) {
    return Collections.emptyList();
  }

}
