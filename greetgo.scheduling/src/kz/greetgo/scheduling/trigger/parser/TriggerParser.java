package kz.greetgo.scheduling.trigger.parser;

import kz.greetgo.scheduling.trigger.SilentTrigger;
import kz.greetgo.scheduling.trigger.Trigger;
import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.parallel_status.TriggerParallelStatus;
import kz.greetgo.scheduling.trigger.parser.parallel_status.TriggerParallelStatusParser;
import kz.greetgo.scheduling.trigger.struct.TriggerParserStructuring;
import lombok.NonNull;

import java.util.Collections;
import java.util.List;

public class TriggerParser {

  private static final TriggerParseResult silentResult = new TriggerParseResult() {
    final Trigger silent = new SilentTrigger();

    @Override
    public Trigger trigger() {
      return silent;
    }

    @Override
    public List<TriggerParseError> errors() {
      return Collections.emptyList();
    }
  };

  public static TriggerParseResult parse(String triggerString) {
    if (triggerString == null) {
      return silentResult;
    }

    if (isCommented(triggerString)) {
      return silentResult;
    }

    TriggerParallelStatus triggerParallelStatus = TriggerParallelStatusParser.parse(triggerString);

    if (!triggerParallelStatus.errors().isEmpty()) {
      return silentResult(triggerParallelStatus.errors());
    }

    TriggerParserStructuring struct = TriggerParserStructuring.of(triggerParallelStatus.range(), triggerString);

    struct.makeResult();

    if (struct.result != null && struct.errors.isEmpty() && !struct.result.isDotty()) {
      return notDottyError(triggerString);
    }

    return new TriggerParseResult() {
      final Trigger trigger = wrap(triggerParallelStatus, struct.result);

      @Override
      public Trigger trigger() {
        return trigger;
      }

      @Override
      public List<TriggerParseError> errors() {
        return struct.errors;
      }

    };
  }

  private static Trigger wrap(TriggerParallelStatus tps, Trigger input) {
    return new Trigger() {
      @Override
      public boolean isHit(long schedulerStartedAtMillis, long timeMillisFrom, long timeMillisTo) {
        return input.isHit(schedulerStartedAtMillis, timeMillisFrom, timeMillisTo);
      }

      @Override
      public boolean isDotty() {
        return input.isDotty();
      }

      @Override
      public boolean isParallel() {
        return tps.isParallel();
      }

      @Override
      public Integer maxParallelSize() {
        return tps.maxParallelSize();
      }

      @Override
      public String toString() {
        return input.toString();
      }
    };
  }

  private static @NonNull TriggerParseResult notDottyError(String triggerString) {
    return new TriggerParseResult() {
      final Trigger silent = new SilentTrigger();

      @Override
      public Trigger trigger() {
        return silent;
      }

      @Override
      public List<TriggerParseError> errors() {
        return Collections.singletonList(new TriggerParseError() {
          @Override
          public String errorMessage() {
            return "Расписание должно указывать моменты времени, а не промежутки";
          }

          @Override
          public String errorCode() {
            return "h180sws";
          }

          @Override
          public Range errorPlace() {
            return Range.of(0, triggerString.length());
          }

          @Override
          public String triggerString() {
            return triggerString;
          }
        });
      }
    };
  }

  private static @NonNull TriggerParseResult silentResult(List<TriggerParseError> errors) {
    return new TriggerParseResult() {
      final Trigger silent = new SilentTrigger();

      @Override
      public Trigger trigger() {
        return silent;
      }

      @Override
      public List<TriggerParseError> errors() {
        return errors;
      }
    };
  }

  private static boolean isCommented(String triggerString) {
    return triggerString == null || triggerString.trim().startsWith("#")
      || triggerString.trim().toLowerCase().startsWith("off");
  }
}
