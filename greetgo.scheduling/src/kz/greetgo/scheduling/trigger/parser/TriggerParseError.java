package kz.greetgo.scheduling.trigger.parser;

import kz.greetgo.scheduling.trigger.model.Range;

public interface TriggerParseError {

  String errorMessage();

  String errorCode();

  Range errorPlace();

  String triggerString();

}
