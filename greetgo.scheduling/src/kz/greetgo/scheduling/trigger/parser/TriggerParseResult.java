package kz.greetgo.scheduling.trigger.parser;

import kz.greetgo.scheduling.trigger.Trigger;

import java.util.List;

public interface TriggerParseResult {

  Trigger trigger();

  List<TriggerParseError> errors();

}
