package kz.greetgo.scheduling.trigger.parser.parallel_status;

import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.TriggerParseError;

import java.util.List;

public interface TriggerParallelStatus {

  Range range();

  boolean isParallel();

  Integer maxParallelSize();

  List<TriggerParseError> errors();

}
