package kz.greetgo.scheduling.trigger.parser.parallel_status;

import kz.greetgo.scheduling.trigger.model.Range;
import kz.greetgo.scheduling.trigger.parser.TriggerParseError;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

public class TriggerParallelStatusParser {

  public static TriggerParallelStatus parse(@NonNull String triggerString) {
    int i1 = -1, i2 = -1;

    for (int i = 0, len = triggerString.length(); i < len; i++) {
      boolean isWhitespace = Character.isWhitespace(triggerString.charAt(i));

      if (isWhitespace && i1 == 0) {
        break;

      } else {
        i2 = i;

        if (i1 < 0) {
          i1 = 0;
        }
      }
    }

    if (i1 < 0) {
      return getSequenceParseStatus(triggerString);
    }

    String firstWord = triggerString.substring(i1, i2 + 1).toLowerCase();

    if (firstWord.startsWith("парал") || firstWord.equals("parallel")) {
      return getParallelParseStatus(triggerString, i2);
    }

    return getSequenceParseStatus(triggerString);
  }

  private static @NonNull TriggerParallelStatus getSequenceParseStatus(@NonNull String triggerString) {
    return new TriggerParallelStatus() {
      final List<TriggerParseError> errors = new ArrayList<>();
      final Range                   range  = Range.of(0, triggerString.length());

      @Override
      public Range range() {
        return range;
      }

      @Override
      public boolean isParallel() {
        return false;
      }

      @Override
      public Integer maxParallelSize() {
        return null;
      }

      @Override
      public List<TriggerParseError> errors() {
        return errors;
      }
    };
  }

  private static @NonNull TriggerParallelStatus getParallelParseStatus(String triggerString, int i2) {
    return new TriggerParallelStatus() {
      final int                     fromIndex       = i2 + 1;
      final List<TriggerParseError> errors          = new ArrayList<>();
      final Range                   range           = getParallelRange(triggerString, fromIndex);
      final Integer                 maxParallelSize = getMaxParallelSize(triggerString, fromIndex, errors);

      @Override
      public Range range() {
        return range;
      }

      @Override
      public boolean isParallel() {
        return true;
      }

      @Override
      public Integer maxParallelSize() {
        return maxParallelSize;
      }

      @Override
      public List<TriggerParseError> errors() {
        return errors;
      }
    };
  }

  // package-privet for tests
  static Integer getMaxParallelSize(@NonNull String triggerString,
                                    int parallelEndIndex,
                                    List<TriggerParseError> errors) {
    String pattern = triggerString.substring(parallelEndIndex).trim();

    if ('[' != pattern.charAt(0)) {
      return null;
    }

    int beginIndex = triggerString.indexOf('[') + 1;
    int endIndex   = triggerString.indexOf(']');

    if (endIndex < 0) {
      errors.add(triggerParseError("Квадратные скобки не закрыты: [~",
                                   "ueC1N7556J",
                                   Range.of(beginIndex, triggerString.length()),
                                   triggerString));
      return null;
    }

    String maxParallelSizeStr = triggerString.substring(beginIndex, endIndex);

    if (maxParallelSizeStr.isEmpty()) {
      errors.add(triggerParseError("Пустое значение в квадратных скобках не допустимо.",
                                   "5K2FTzv0ZL",
                                   Range.of(beginIndex, endIndex),
                                   triggerString));
      return null;
    }

    try {
      return Integer.parseInt(maxParallelSizeStr);
    } catch (NumberFormatException e) {
      errors.add(triggerParseError("Недопустимое значение в квадратных скобках: " + maxParallelSizeStr,
                                   "1MtdX44R81",
                                   Range.of(beginIndex, endIndex),
                                   triggerString));
      return null;
    }
  }

  // package-privet for tests
  static @NonNull Range getParallelRange(@NonNull String triggerString, int parallelEndIndex) {
    String pattern = triggerString.substring(parallelEndIndex).trim();

    if ('[' != pattern.charAt(0)) {
      return Range.of(parallelEndIndex, triggerString.length());
    }

    int beginIndex = triggerString.indexOf(']');

    return Range.of(beginIndex + 1, triggerString.length());
  }

  private static @NonNull TriggerParseError triggerParseError(String errorMessage,
                                                              String errorCode,
                                                              Range errorPlace,
                                                              String triggerString) {
    return new TriggerParseError() {
      @Override
      public String errorMessage() {
        return errorMessage;
      }

      @Override
      public String errorCode() {
        return errorCode;
      }

      @Override
      public Range errorPlace() {
        return errorPlace;
      }

      @Override
      public String triggerString() {
        return triggerString;
      }
    };
  }
}
