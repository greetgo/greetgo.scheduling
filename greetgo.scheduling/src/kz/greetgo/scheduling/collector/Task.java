package kz.greetgo.scheduling.collector;

import kz.greetgo.scheduling.trigger.Trigger;

public interface Task {

  String id();

  Trigger trigger();

  Job job();

}
