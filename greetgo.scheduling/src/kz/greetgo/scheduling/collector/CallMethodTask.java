package kz.greetgo.scheduling.collector;

import kz.greetgo.scheduling.trigger.Trigger;
import lombok.NonNull;

public class CallMethodTask implements Task {
  private final String  id;
  private final Trigger trigger;
  private final Job     job;

  private CallMethodTask(String id, Trigger trigger, Job job) {
    this.id      = id;
    this.trigger = trigger;
    this.job     = job;
  }

  public static @NonNull CallMethodTask of(String id, Trigger trigger, Job job) {
    return new CallMethodTask(id, trigger, job);
  }

  @Override
  public String id() {
    return id;
  }

  @Override
  public Trigger trigger() {
    return trigger;
  }

  @Override
  public Job job() {
    return job;
  }

}
